package com.indulgent.jetbrains.plugin.code.comment.view.actions;

import com.indulgent.jetbrains.plugin.code.comment.logging.Log;
import com.indulgent.jetbrains.plugin.code.comment.messaging.MessagingProvider;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.*;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.impl.ErrorMessageBundle;
import com.indulgent.jetbrains.plugin.code.comment.model.group.GroupInfoServiceFactory;
import com.indulgent.jetbrains.plugin.code.comment.model.user.UserInfo;
import com.indulgent.jetbrains.plugin.code.comment.model.user.UserInfoServiceFactory;
import com.indulgent.jetbrains.plugin.code.comment.view.component.CommentToolWindowFactory;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class EditCommentAction extends AnAction {

	@Override
	public void actionPerformed(AnActionEvent anActionEvent) {

		Project project = anActionEvent.getProject();
		if (project == null) {
			Messages.showInfoMessage("Comments not selected.", "Attention");
			return;
		}

		Collection<Comment> selectedComments = CommentToolWindowFactory.isActiveToolWindow(project) ? CommentToolWindowFactory.getToolWindowComments(project) : getEditorComments(project);

		if (selectedComments.size() == 1) {
			Comment oldComment = selectedComments.toArray(new Comment[]{})[0];
			Comment newComment = selectedComments.toArray(new Comment[]{})[0];

			UserInfo currentUser = UserInfoServiceFactory.getService(project).getCurrentUser();

			String commentText = Messages.showMultilineInputDialog(project, "Comment to:\n" + oldComment.getCodeInformation().getCodeText(), "Comment", getLatestCommentText(oldComment), AllIcons.General.Balloon, null);
			if (commentText == null || commentText.length() == 0) {
				return;
			}

			newComment.getCommentHistory().add(currentUser, commentText, Calendar.getInstance());

			CommentService service = ServiceManager.getService(project, CommentService.class);
			try {
				service.edit(oldComment, newComment);
				MessagingProvider.getInstance(project).sendAddComments(Collections.singletonList(newComment));
			} catch (Exception e) {
				Log.getInstance(project).error(ErrorMessageBundle.message(e.getClass().getName()), e);
			}

		}


	}

	private String getLatestCommentText(Comment comment) {
		int size = comment.getCommentHistory().getRecords().size();
		return comment.getCommentHistory().getRecords().toArray(new CommentHistoryRecord[]{})[size - 1].getText();
	}

	private List<Comment> getEditorComments(Project project) {
		Messages.showInfoMessage("Comments not selected.", "Attention");
		return Collections.emptyList();
	}
}
